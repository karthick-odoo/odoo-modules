# -*- coding: utf-8 -*-

from odoo import models, fields, api

class Region(models.Model):
    _name = 'regions'

    #region_id = fields.Integer(string = "Region_id" , required = True)
    region_name = fields.Text(string = "Region_name" , required = True)


	
class Country(models.Model):
    
    _name = 'countries'
   
    #country_id   = fields.Integer(string = "Country_id", required = True)
    country_name = fields.Text(string = "Country_name", required = True)
    #region_id    = fields.Integer(string = "Region_id", required = True)


class Location(models.Model):
    
    _name = "locations"
   
    street_address = fields.Text(string = "location_name", required = True)
    postal_code    = fields.Integer(required = True)
    city           = fields.Text(required = True)
    state_province = fields.Char(required = True)
    

class Department(models.Model):
  
    _name = 'departments'
 
    department_name = fields.Text(string = 'department_name', required = True)
    

class employee(models.Model):

    _name = 'employees'
 
    first_name = fields.Text(string = 'first_name', required = True)
    last_name = fields.Text (string = 'second_name', required = True)
    email       = fields.Text(required = True)
    phone_number = fields.Integer(required = True)
    hire_date   = fields.Date(required = True)
    salary     = fields.Float(required = True)
    commission_pct = fields.Integer(required = True)
    
class job(models.Model):
 
    _name = 'jobs'

    job_title = fields.Text(required = True)
    min_salary = fields.Integer(required = True)
    max_salary = fields.Integer(required = True)
    
class job_grade(models.Model):

    _name = 'job_grades'

    grade_level = fields.Char(required = True)
    lowest_salary = fields.Float(required = True)
    highest_salary = fields.Float(required = True)

class job_history(models.Model):

    _name = 'job_history'

    start_date = fields.Date(required = True)
    end_date = fields.Date(required = True)
    

    




